import Proj1 from "../assets/proj1.png";
import Proj2 from "../assets/proj2.png";
import Proj3 from "../assets/proj3.png";
import Proj4 from "../assets/proj4.png";
import Proj5 from "../assets/proj5.png";

export const ProjectList = [
  {
    name: "Single page food website application",
    image: Proj1,
    skills: "JavaScript,HTML,CSS",
    demo: "https://food-app-react-js.vercel.app/",
    code: "https://gitlab.com/zuitt-196/food-app-react-js"
  },
  {
    name: "Enventory Managemment Application",
    image: Proj2,
    skills: "React,Node.js,MongoDB",
  },
  {
    name: "asdasdsads",
    image: Proj3,
    skills: "React,Node.js,MongoDB,SpotifyAPI",
  },
  {
    name: "My school website Application",
    image: Proj4,
    skills: "React,Node.js,MySQL,GraphQL",
  },
  {
    name: "Tesla clone Application",
    image: Proj5,
    skills: "JavaScript,HTML,CSS",
  },
  
];
