import React from "react";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import EmailIcon from "@material-ui/icons/Email";
import GithubIcon from "@material-ui/icons/GitHub";
import "../styles/Home.css";

function Home() {
  return (
    <div className="home">
      <div className="about">
        <h2> Hi, My Name is Vhong</h2>
          <div className="home-image">
              <img src="https://scontent.fceb1-2.fna.fbcdn.net/v/t1.15752-9/330637161_5840454566049841_2229886556349479516_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeHIqZ4oa-pTyKqLMTTvTCwUVhW7BhJEy75WFbsGEkTLvpOSiC2cLMTOfd5e88gjNayL5YRZi4olbuHOmkuVR0oY&_nc_ohc=cwbORw7SxyoAX9N76lR&_nc_ht=scontent.fceb1-2.fna&oh=03_AdSZdNCSzwUrfXDIJNHNf15XvZRRt8XRtMnMgT2o3gVoPQ&oe=6410273E" alt="" />
          </div>
        <div className="prompt">
          <p>A Web developer with a passion for learning and creating.</p>
          <a href="https://www.facebook.com/bongbong.bercasio.9"><LinkedInIcon/></a>
         <a href="https://www.linkedin.com/in/bongbong-bercasio-5b1332245/"><EmailIcon /></a> 
              <a href="https://gitlab.com/bercasiobong"><GithubIcon /></a>
        </div>
      </div>

      <div className="skills">
        <h1> Skills</h1>
        <ol className="list">
          <li className="item">
            <h2> Front-End</h2>
            <span>
              ReactJS, HTML, CSS , Bootstrap, 
            </span>
          </li>
          <li className="item">
            <h2>Back-End</h2>
            <span>
              NodeJS ExpressJS, MongoDB
            </span>
          </li>
          <li className="item">
            <h2>Languages</h2>
            <span>JavaScript</span>
          </li>
        </ol>
      </div>
    </div>
  );
}

export default Home;
